# Angular 1.5.7 ES6 project starter

This is a project starter for an AngularJS 1.5 web app. It is set to work with the following technologies:

* **ECMAScript 6** as version of Javascript to take advantage of module concept and enhance code clarity with class syntax,
* **Babel** as ES6 to ES5 transpiler,
* **JSHint** as code quality checker,
* **LESS** as base language for styles,
* **SystemJS** as module loader,
* **JSPM** as application dependency package manager,
* **NPM** as project's tool package manager,
* **Gulp** as task runner,
* **Karma** as spec runner,
* **Jasmine** as framework for Unit Testing,
* **PhantomJS** and **Chrome** as testing browsers, PhantomJS is the default activated browser but Chrome is preset on the project configuration so you can switch to Chrome for convenience if you want.

## Features

The starter is more than just a skeleton, it comes with few basic features that are intended to be completed with your own needs:

* **[Template](src/app/components/template/README.md)** - allows you to manage your identity guidelines in one place
* **[Page service](src/app/services/page/README.md)** - allows you to manage your page configurations anywhere in the app
* **[Context service](src/app/services/context/README.md)** - allows you to store contextual data
* **[Locator factory](src/app/services/locator/README.md)** - allows you to have your api urls in one place

## Getting Started

You can simply clone the angular-starter repository, remove the other starter versions (if any) and install the dependencies:

### Prerequisites

You need git to clone the angular-seed repository. you can get git from [http://git-scm.com/](http://git-scm.com/).
We also use a number of NodeJs tools to initialize and test the project. You must have NodeJs and NPM installed. You can get them from [http://nodejs.org/](http://nodejs.org/).

### Clone angular-starter

Clone the angular-starter repository using git:

```
git clone https://bitbucket.org/astrinxit66/angular-starter.git
cd angular-starter
```

The clone will make a copy of each starter version onto your machine, you can just pick up and rename the `1.5.7-es6` folder as a base project and remove any other versions.

### Install Dependencies

We have two kinds of dependencies in this project: tools and application. The tools help us manage our development environnement, we are using them for tests, builds, and code quality check for examples. 

* we get the tools we depend upon via `npm`,
* we get the application dependencies via `jspm`.

We need to install jspm globally if it's not yet your case:

```
npm install -g jspm
```

*NB: you may have to execute this command with administrator privileges if you encounter any error*

Idem for the gulp tool:

```
npm install -g gulp
```

*NB: you may have to execute this command with administrator privileges if you encounter any error*

For the rest the project package is preconfigured to automatically install application dependencies so we can simply do:

```
npm install
```

*NB: you may have to execute this command with administrator privileges if you encounter any error*

The above command will create 2 new folders in your project:

* `node_modules` - contains the npm packages for the tools we need,
* `jspm_packages` - contains the application dependencies (including the angular framework files)

### Serving the application

We use `gulp` for serving, testing and building our app. Serving an application consists in mounting a development web server and running your app on that web server. We have 2 kinds of serve tasks:

* `gulp serve:dev` - allow you to functionally test your application and technically debug it. Code transpilation are made on the fly and LESS source code parsed the same way. The web server is also provided with live reload feature so that your app will automatically refresh on the browser on code changes (set to be JS code changes only),
* `gulp serve:prod` - allow you to test your app in _production mode_ like where your source code is bundled and LESS parsed into CSS on its own file, etc.

Each `serve` task check code quality and run specs before mounting the web server. Any code quality weakness or spec failure will cause the process to stop.  
The web server will serve the app through `http://localhost:9000`, the process will automatically open a browser for you when ready.

### Testing

Unit tests are written in Jasmine and they are found next to the code they are testing, with their name prefixed with `-spec.js`.

You can run test task with `gulp test`.

### Directory structure

```
src/                ---> all of the source files for the application
   app/             ---> main application folder
      components/       ---> all app specific modules
         hello/             ---> [REMOVABLE] example of business module
            hello.controller.js    ---> the hello component controller
            hello.html             ---> the partial for the hello component
            hello.js               ---> the hello module definition
            hello.settings.js      ---> the hello module configuration
         template/          ---> template module folder
            template.controller.js      ---> template controller
            template.controller-spec.js ---> template controller spec
            template.html               ---> template partial
            template.js                 ---> template module definition
            template.less               ---> template style file
            template.skin.less          ---> public template style file
         components.js      ---> here is where components are imported
      services/         ---> all app specific services
         context/           ---> context service folder
            context.service.js          ---> context service implementation
            context.service-spec.js     ---> context service spec
         locator/           ---> locator factory folder
            locator.factory.js          ---> locator factory implementation
         page/              ---> page service folder
            page.provider.js            ---> page provider implementation
            page.service.js             ---> page service implementation
            page.service-spec.js        ---> page service spec
         services.js        ---> service module definition
      app.js            ---> main application module
      bootstrap.js      ---> app initializer
      properties.js     ---> constant container
   assets/          ---> images, fonts, etc.
   e2emock/         ---> backend mock folder
      mocks/            ---> business mocks folder
         base.mock.js       ---> base class of business mocks
         hello.mock.js      ---> [REMOVABLE] example of business mock implementation
      resources/        ---> list of available resource helper
         resources.js       ---> class that aggregate available resources
         storage.resource.js --> storage resource
      e2emock.js        ---> backend mock initializer
   index.html       ---> app layout file (default entry point to access the app)
  
```