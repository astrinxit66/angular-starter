/**
 * Created by jean-marc on 21/07/2016.
 * @module e2emock/e2emock
 */
/*jshint esversion: 6*/
"use strict";

import "angular-mocks";

import "../app/app";

import MODULE_NAME from "../app/properties";
import HelloMock from "./mocks/hello.mock";

export default angular
    .module("appMock", [
        "ngMockE2E",

        //here is where the real app is loaded
        MODULE_NAME.APP
    ])

    .run( bootBackendMock );

bootBackendMock.$inject = ["$httpBackend","locatorFactory"];
function bootBackendMock( $backend, locatorFactory ){

    //deliberately force jshint to ignore 
    //the following statements as we know
    //that there is no "side effect" about
    //this.
    /*jshint ignore:start*/
    new HelloMock($backend, locatorFactory );
    /*jshint ignore:end*/

    // pass through unresolved requests
    $backend.whenGET().passThrough();
    $backend.whenPOST().passThrough();
    $backend.whenDELETE().passThrough();
}