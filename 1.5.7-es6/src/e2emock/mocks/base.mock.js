/**
 * Created by jean-marc on 21/07/2016.
 * @module e2emock/mocks/BaseMock
 */
/*jshint esversion: 6*/
"use strict";

import MockResource from "../resources/resources";

/**
 * BaseMock serves as a base class for mocks. It resolves
 * automatically incoming requests and call the appropriate
 * response method. It also provides a set of resources that
 * business mock classes can use (storage, etc.)
 * @extends MockResource
 */
export default class BaseMock extends MockResource {

    /**
     * Register each request found on locatorManifest into the $httpBackend
     * tracked list and trigger the associated response method when a request
     * matches.
     * This allow business mock classes to not care about handling requests.
     *
     * @param {string} $mockIdentifier
     *      An arbitrary name to identify the business mock class that is using
     *      the BaseMock
     * @param {HttpBackend} $backend
     *      Shared $httpBackend
     * @param {Array<Object>} locatorManifest
     *      A collection of json object which contain the urls that the mock class wants
     *      to intercept. Each object properties:
     *      type: GET|POST|DELETE,
     *      url: the url to be intercepted,
     *      response: the name of the method that will respond to the request
     */
    constructor($mockIdentifier, $backend, locatorManifest ){

        super( $mockIdentifier );

        for(let request of locatorManifest){

            $backend[`when${request.type}`]( this.haveUrl( request.url ) )
                .respond((...params) => {

                    //auto log for debug
                    this.log(`${request.url} caught and treated by ${request.response}`);

                    //delegate business treatment of the response to the corresponding
                    //method in child class (business mock class)
                    return this[ request.response ].bind(this)(...params);
                });
        }
    }

    /**
     * Removes the GET parameters from url if any and 
     * return the base url
     * @param url
     * @return string
     */
    getNonParametrizedUrl( url ){
        return url.split("?").shift();
    }

    /**
     * Provide a pseudo-function that challenges the url in its argument
     * with the given expected url
     * @param {string} expectedUrl
     * @return {Function}
     */
    haveUrl( expectedUrl ){
        return ( url ) => this.getNonParametrizedUrl( url ) === expectedUrl;
    }
}