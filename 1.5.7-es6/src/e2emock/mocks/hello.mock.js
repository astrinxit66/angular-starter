/**
 * Created by jean-marc on 21/07/2016.
 * @module e2emock/mocks/HelloMock
 */
/*jshint esversion: 6*/
"use strict";

import BaseMock from "./base.mock";

/**
 * Example of a mock class
 * @extends BaseMock
 */
export default class HelloMock extends BaseMock {

    constructor($backend, locatorFactory ){
        let manifest = [{
            type: "GET",
            url: locatorFactory.hello,
            response: "getHello"
        }];

        super("Hello", $backend, manifest);
    }

    /**
     * Response method to a GET request on
     * locatorFactory.hello
     *
     * @param method    request method
     * @param url       request url
     * @param data      request data
     * @param headers   request headers
     * @param params    request params
     * @returns Array   response
     */
    getHello(method, url, data, headers, params){
        let responseBody = {message: "Hello from E2EMock!"};

        //example of using the storage resource
        this.storage.setItem("message", JSON.stringify( responseBody ));

        return [200, responseBody];
    }
}