/**
 * Created by jean-marc on 21/07/2016.
 * @module e2emock/resources/storage.resource
 */
/*jshint esversion: 6*/
"use strict";

import StorageResource from "./storage.resource";

//memoized class
/** @type StorageResource */
let $$storage = null;

/**
 * Class representing the available resources
 * for mock classes
 */
export default class MockResource {

    /**
     * Register resources for mock class
     * @param mockIdentifier:string    The identifier of the mock class that uses this
     */
    constructor( mockIdentifier ){
        this.identifier = mockIdentifier;
        
        let resource = this;

        //available resources:
        resource.storage = $$storage === null ? new StorageResource( mockIdentifier ) : $$storage;
    }

    /**
     * A logger that identifies for each log the mock from which it comes from
     * @param message:string
     * @param data:{null|object}
     */
    log(message, data = null){
        let msg = `[ngMock|${this.identifier}] ${message}`;

        if( data !== null ){
            console.log(msg, data);
        } else {
            console.log( msg );
        }
    }
}