/**
 * Created by jean-marc on 21/07/2016.
 * @module e2emock/resource/StorageResource
 */
/*jshint esversion: 6*/
"use strict";

//our storage is currently based on HTML5 sessionStorage
let $$storage = window.sessionStorage;

/**
 * Class representing the default resource
 * that mocks can use to save data.
 */
export default class StorageResource {

    /**
     * @param mockIdentifier:string
     */
    constructor( mockIdentifier ){

        this.mockId = mockIdentifier;
    }

    //use the same method as provided by the HTML5 Storage interface
    // but just prepend keys with specific mock context
    setItem(key, value){ return $$storage.setItem(`mock-${this.mockId}-${key}`, value); }
    getItem(key, value){ return $$storage.getItem(`mock-${this.mockId}-${key}`, value); }
    removeItem( key ){ return $$storage.removeItem( `mock-${this.mockId}-${key}` ); }
}