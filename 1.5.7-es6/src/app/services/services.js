/**
 * Created by jean-marc on 21/07/2016.
 */
/*jshint esversion: 6*/
"use strict";

import MODULE_NAME from "../properties";

import ContextService from "./context/context.service";
import PageProvider from "./page/page.provider";
import locatorFactory from "./locator/locator.factory";

export default angular

    .module(MODULE_NAME.SERVICES, [])

    /**
     * locatorFactory handle all the request url
     * that can be used through the app. This is preferable
     * for urls' maintainability and minification where there
     * is only one string value per request
     */
    .factory("locatorFactory", locatorFactory)

    /**
     * PageService allow your app to have a concept of pages.
     * A page has some info that other components can use
     * (eg the title is used by the tpl-skin to set the title of
     * the window)
     */
    .provider("PageService", PageProvider)

    /**
     * ContextService offers capability for your modules to set
     * sessions and store data into the HTML5 database
     */
    .service("ContextService", ContextService);