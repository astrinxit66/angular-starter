/**
 * Created by jean-marc on 04/08/2016.
 * @module app/services/context/ContextServiceSpec
 */
/*jshint esversion: 6*/
"use strict";

import mock from "angular-mocks";

import MODULE_NAME from "../../properties";

import "../services";

describe("ContextService", () => {
    let service = null, $rootScope = null, $location = null;

    beforeEach(() => {
        mock.module(MODULE_NAME.SERVICES);

        mock.inject((ContextService, _$location_, _$rootScope_) => {
            service = ContextService;
            $location = _$location_;
            $rootScope = _$rootScope_;
        });
    });

    //Flash data check
    describe("1. Check flash data functionality", () => {
        let dataTestCase = null;

        beforeEach(() => {
            dataTestCase = {key: "fdTest", message: "Hello World"};
        });

        it("should set flash data for specific route", addFlashDataTest);
        it("should not be able to read flash data twice", flashDataUnicityTest);
        ////////////////

        function addFlashDataTest(){
            //flash data on current location
            $location.path("/flash-data-test");
            service.addFlashData(dataTestCase.key, dataTestCase);
            expect( service.getFlashData( dataTestCase.key ) ).toEqual( dataTestCase );

            //flash data on different location
            let location1 = "/flash-data-location-1",
                location2 = "/flash-data-location-2";

            $location.path( location1 );
            service.addFlashData( dataTestCase.key, dataTestCase, location2);
            expect( service.getFlashData( dataTestCase.key ) ).toBe( null );

            $location.path( location2 );
            expect( service.getFlashData( dataTestCase.key ) ).toEqual( dataTestCase );
        }

        function flashDataUnicityTest(){
            $location.path("/flash-data-location-3");
            service.addFlashData( dataTestCase.key, dataTestCase );
            expect( service.getFlashData( dataTestCase.key ) ).toEqual( dataTestCase );
            expect( service.getFlashData( dataTestCase.key ) ).toBe( null );
        }
    });

    //Local/Session data check
    describe("2. Check local and session data functionality", () => {
        let keysToClear = [];

        afterEach(() => {
            for(let entry of keysToClear ){
                service.removeLocalData( entry );
                service.removeSessionData( entry );
            }

            keysToClear.length = 0;
        });

        it("should isolate stored data by location", dataIsolationTest);
        it("should preserve the type of stored data when retrieved", preserveObjectTypeTest);
        //////////////////

        function dataIsolationTest(){
            let entry = "test";

            $location.path("/loc-1");
            service.addLocalData(entry, "test");
            service.addSessionData(entry, "session test");
            expect( service.getLocalData( entry ) ).toEqual("test");
            expect( service.getSessionData( entry ) ).toEqual("session test");

            $location.path("/loc-2");
            expect( service.getLocalData( entry ) ).toBe( null );
            expect( service.getSessionData( entry ) ).toBe( null );

            keysToClear.push( entry );
        }

        function preserveObjectTypeTest(){
            let entry = "a";

            $location.path("/loc-3");

            service.addLocalData(entry, "test");
            expect( service.getLocalData( entry ) ).toEqual("test");

            service.addLocalData(entry, false);
            expect( service.getLocalData( entry ) ).toBe( false );

            service.addLocalData(entry, 25.2);
            expect( service.getLocalData( entry ) ).toBe( 25.2 );

            let obj = {prop1: "val1"};
            service.addLocalData(entry, obj);
            expect( service.getLocalData( entry ) ).toEqual( obj );

            let arr = [true, {prop1: "val1"}, "a string", -18, 0.765];
            service.addLocalData(entry, arr);
            expect( service.getLocalData( entry ) ).toEqual( arr );

            keysToClear.push( entry );
        }
    });
});