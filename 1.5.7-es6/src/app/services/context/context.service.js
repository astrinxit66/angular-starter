/**
 * Created by jean-marc on 24/07/2016.
 * @module app/services/context/ContextService
 */
/*jshint esversion: 6*/
"use strict";

let $$location = null,
    $$storage = {local: window.localStorage, session: window.sessionStorage},
    $$flashSession = {};

const STORAGE_TYPE = {FLASH: 1, LOCAL: 2, SESSION: 3};

/**
 * The ContextService offers a local storage manager. You can use
 * it for storing data for
 * - a session lifetime (the time the current browser window is open)
 * - long term lifetime (no time limitation)
 * - a flash session lifetime (one access of the data).
 *
 * The ContextService is aware of the component module from which
 * data are set and get. This allow components to have their own context
 * isolated from others.
 *
 * Data are kept using HTML5 Storage interface to optimize memory usage.
 * Remember that the Storage interface is limited to 5Mb of data per domain.
 */
export default class ContextService {

    constructor($rootScope, $location){

        //ensure both local and session storage
        //are available
        this._requireStorage();

        $$location = $location;

        //release non relevant data on module leave
        $rootScope.$on("$routeChangeSuccess", this._moduleChanged.bind(this));
    }

    _storageGetter(storage, key){
        let stringValue = $$storage[ storage ].getItem( this._contextualizedKey( key ) );

        if( stringValue === null ){
            return stringValue;
        }

        try {
            /**
             * JSON.parse covers object/array casting
             * and boolean casting
             */
            return JSON.parse( stringValue );
        } catch( e ){
            if( angular.isNumber( stringValue ) ){
                return Number( stringValue );
            }

            return stringValue;
        }
    }

    _contextualizedKey( key ){
        let namespace = "cs",
            moduleIdentifier = $$location.path().charAt(0) === "/" ? $$location.path().substring( 1 ) : $$location.path();

        return `${namespace}-${moduleIdentifier}-${key}`;
    }

    /**
     *
     * @param type:STORAGE_TYPE
     * @param value
     * @private
     */
    _createOrRead(type, value){
        let storage = null;

        switch( type ){
            case STORAGE_TYPE.FLASH: storage = $$flashSession; break;
            case STORAGE_TYPE.LOCAL: storage = $$storage.local; break;
            case STORAGE_TYPE.SESSION: storage = $$storage.session; break;
            default: throw new Error("Unresolved storage type");
        }

        if( !storage.hasOwnProperty( value ) ){
            storage[ value ] = {};
        }

        return storage[ value ];
    }

    _requireStorage(){
        try {
            for(let storageType in $$storage){
                $$storage[ storageType ].setItem("a", "1");
                $$storage[ storageType ].removeItem("a");
            }
        } catch(e) { //Storage is not available
            throw new Error("HTML5 Storage is not available");
        }
    }

    /**
     * Release previous module associated flash session as they perish on module leave,
     * Init next module flash session if any,
     *
     * @param event
     * @param nextRoute
     * @param previousRoute
     * @private
     */
    _moduleChanged(event = null, nextRoute = null, previousRoute = null){
        let currentModulePath = nextRoute ? nextRoute.$$route.originalPath : null,
            outdatedModulePath = previousRoute ? previousRoute.$$route.originalPath : null;

        //release unused flash data
        if( outdatedModulePath && $$flashSession.hasOwnProperty( outdatedModulePath ) ) {
            delete $$flashSession[ outdatedModulePath ];
        }

        //init a flash session for the current module
        if( currentModulePath ){
            this._createOrRead(STORAGE_TYPE.FLASH, currentModulePath);
        }
    }

    /**
     * Add a data that can be accessed once. Flash data
     * can be set on specified module using moduleLocation.
     * @param key
     * @param value
     * @param locationPath
     */
    addFlashData(key, value, locationPath = null){
        this._createOrRead(STORAGE_TYPE.FLASH, locationPath !== null ? locationPath : $$location.path())[ key ] = value;
    }

    getFlashData( key ){

        let data = $$flashSession[ $$location.path() ] && $$flashSession[ $$location.path() ].hasOwnProperty( key ) ? $$flashSession[ $$location.path() ][ key ] : null;

        if( data !== null ){
            this.removeFlashData( key );
        }

        return data;
    }

    removeFlashData( key ){
        if( !$$flashSession[ $$location.path() ] || !$$flashSession[ $$location.path() ].hasOwnProperty( key ) ){
            return;
        }

        delete $$flashSession[ $$location.path() ][ key ];

        if( Object.keys( $$flashSession[ $$location.path() ] ).length === 0 ){
            delete $$flashSession[ $$location.path() ];
        }
    }

    addLocalData(key, value){
        let contextualKey = this._contextualizedKey( key ),
            stringValue = angular.isObject( value ) ? JSON.stringify( value ) : value;

        $$storage.local.setItem(contextualKey, stringValue);
    }

    getLocalData( key ){
        return this._storageGetter("local", key);
    }

    removeLocalData( key ){
        $$storage.local.removeItem( this._contextualizedKey( key ) );
    }

    addSessionData(key, value){
        let contextualKey = this._contextualizedKey( key ),
            stringValue = angular.isObject( value ) ? JSON.stringify( value ) : value;

        $$storage.session.setItem(contextualKey, stringValue);
    }

    getSessionData(key){
        return this._storageGetter("session", key);
    }

    removeSessionData(key){
        $$storage.session.removeItem( this._contextualizedKey( key ) );
    }
}
ContextService.$inject = ["$rootScope","$location"];