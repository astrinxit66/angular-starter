# Angular 1.5.7 ES6 project starter

## ContextService

The ContextService can be considered as a wrapper of the DOM Storage API that ensures stringifying your objects for you and retrieving it back into its former type when needed. It provides support for `sessionStorage` and for `localStorage` as well. Here are the available methods for these API:

##### Local storage

* `ContextService.addLocalData(key, value)` - like _localStorage.addItem(key, value)_
* `ContextService.getLocalData( key )` - like _localStorage.getItem( key )_
* `ContextService.removeLocalData( key )` - like _localStorage.removeItem( key )_ 

##### Session storage

* `ContextService.addSessionData(key, value)` - like _sessionStorage.addItem(key, value)_
* `ContextService.getSessionData( key )` - like _sessionStorage.getItem( key )_
* `ContextService.removeSessionData( key )` - like _sessionStorage.removeItem( key )_

##### Flash storage

ContextService also introduce the concept of _flash data_. A flash data is a data that can only be read once. The data is removed after the first read. A flash data is stored as a normal variable.

* `ContextService.addFlashData(key, value [, locationPath])` - store _value_ identified by _key_. The optional _locationPath_ allows you to set the flash data for a specific page
* `ContextService.getFlashData( key )` - allows the current page to read the value of the data identified by _key_ 
* `ContextService.removeFlashData( key )` - allows the current page to remove the data identified by _key_

### Contextualization of data

The ContextService does not only store your data, it guarantees their isolation from others. In other words your data are contextualized to their origin. A data added in _Page1_ cannot be accessed in _Page2_ through the service. Only the flash storage API may derogate to this rule, where you can set a flash data for another page (but you will not be able to read it then, only the intended page will).