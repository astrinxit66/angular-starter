# Angular 1.5.7 ES6 project starter

## PageService & PageServiceProvider

The PageService was designed to serve as page settings container. It can be used by any component that wants to expose its settings or any component that wants to know about the settings of other components. The template component uses it for example to adapt its presentation according to the current displayed page.

##### Registering a component as a page with settings

There is a `PageServiceProvider` and a `register` method that allows you to set your component as a page and register its settings. We can do that on the config section of our module:

```javascript
angular
    .module("myModule", [
        "myModule.services" //assume this is the name of the module where you defined the PageServiceProvider    
    ])
    .component("myPageComponent", {...})
    .config( myPageSettings );
    
myPageSettings.$inject = ["$routeProvider", "PageServiceProvider"];
function myPageSettings($routeProvider, PageServiceProvider){
    let locationPath = "/my-page";
    
    PageServiceProvider.register(locationPath, {
        title: "My page title",
        
        option1: true,
        option2: [
            //some complex objects here if you want
        ]
    });
    
    $routeProvider.when(locationPath, {
        template: "<my-page-component></my-page-component>",
    });
}
```

##### Reading a page settings

Use the `PageService` to get back the options of a specific page. There are 2 methods for that:

* `getOption(option [, locationPath])` - allows you to get the value of the option identified by its name. The default _locationPath_ is the current location path. You can specify the location path of a specific page to get the value option of that page.
* `getOptions([locationPath)` - allows you to get all options of the current page (identified by its location path). You can specify a location path of another page if you want to get the options of that page.

The configuration of your pages can be done with the `PageServiceProvider` provider, you can then access these settings from anywhere in your app with the `PageService` service.

##### Ready state of a page

With PageService a page can express that it is in ready state. You can set the ready state of a page using `PageService.ready([locationPath])` or you can react when a page is ready with `PageService.whenReady([locationPath])` which returns a promise.