# Angular 1.5.7 ES6 project starter

## Template  

The template module allows you to manage your identity guidelines.

### Template Partial
 
You define once the visual components of your app (such as header, footer, menu bar, left/right panels, modal, etc.) and they will automatically wrap your modules' content. There is 2 files involved in the template partial: 

* `template.html` - represents the view
* `template.less` - contains the styles associated with the view

Actually, there is one another file, `template.skin.less`, that is designed to contain the classes that you want to reuse in your other modules to preserve your visual identity.

### Template behaviour

You may want your template to be slightly different on some of your component's view. This is possible thanks to the [`PageServiceProvider`](../../services/page/README.md) with which you can override the default template parameters for your component. For that, you just need to register your component location along with the parameters with the [`PageServiceProvider.register`](../../services/page/README.md) method.

Hence done, the template will adapt its presentation to the parameters you've set when the user arrives on your component view.

##### Let's illustrate this by taking a scenario of a possible behaviour:
Assume that we have 3 page types in our app, the first one `Auth` is an authentication page with only a login form in the middle of the screen (no header, no footer, no menu bar, nothing else), the second type `Dashboard` is a dashboard page that differs from the last one by providing a left panel filled with widgets, and the third one `Common` is a common content's container that contains a header, a footer and a menu bar.
   
The pages are registered on [`PageServiceProvider.register`](../../services/page/README.md) with the following options:

```json
/**
* All of these are hypothetical
*/
//Auth options
{
    title: "Authentication page title",
    enableHeader: false, //overrides default value (true)
    enableFooter: false //overrides default value (true)
    enableMenuBar: false, //overrides default value (true)
}
 
//Dashboard options
{
    title: "Dashboard page title",
    enableLeftPanel: true,  //overrides default value (false)
    widgetCollection: [
        "<my-widget1></my-widget1>",
        "<my-widget2></my-widget2>"
    ]
}

//Common options
{
     title: "Common page title"
}
```

Finally let's assume that we have done the necessary job on the PageServiceProvider, the template controller and the template partial to support all these options (yes, you need to implement the behaviour that you want your template to have). Then the template behaviour will be:

1. When user arrive on the Auth page, the template component disables the header, footer and the menu bar according to the bellow options. It also set the browser's window title to _"Authentication page title"_.
2. When he falls on the Dashboard page, the template component enables header, footer, menu bar and the left panel. It will update the window title to _"Dashboard page title"_. It will also walk through the `widgetCollection` list and inject each found component into the left panel automatically.
3. When the user opens a Common page, the template component disables left panel and its widgets. The page then now contains the header, the footer and the menu bar. The window title becomes _"Common page title"_.

### Template's custom components

The template module component is the best place to create your reusable components, such as a _modal component_ or a _generic widget component_.