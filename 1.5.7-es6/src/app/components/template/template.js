/**
 * Created by jean-marc on 21/07/2016.
 * @module app/components/template/template
 *
 * Template module definition. Here we can define
 * the template component and all transversal components
 * that may by associated to the template.
 */
/*jshint esversion: 6*/
"use strict";

import MODULE_NAME from "../../properties";

//import style
import "./template.less!";

import TemplateController from "./template.controller";
import templateTemplate from "./template.html!ng-template";

export default angular

    .module(MODULE_NAME.TEMPLATE, [])

    .component("appTemplate", Object.assign(
        templateTemplate,
        {
            controller: TemplateController
        }
    ));