/**
 * Created by jean-marc on 21/07/2016.
 * @module app/components/components
 *
 * Here are imported our components
 */
/*jshint esversion: 6*/

/**
 * Template component
 * This is part of the proposed feature
 * and should not be removed then.
 * (not an example)
 */
import "./template/template";

/**
 * Hello component
 * This is an example of how to define
 * a component-module that acts like a
 * page.
 * You can remove this and the corresponding
 * folder on your project
 */
import "./hello/hello";