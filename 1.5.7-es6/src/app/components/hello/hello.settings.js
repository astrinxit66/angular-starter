/**
 * Created by jean-marc on 24/07/2016.
 * @module app/components/hello/helloSettings
 */
/*jshint esversion: 6*/
"use strict";

export default helloSettings;

helloSettings.$inject = ["$routeProvider","PageServiceProvider"];
function helloSettings($routeProvider, PageServiceProvider){
    let locationPath = "/";

    PageServiceProvider.register(locationPath, {
        enableHeader: true,
        enableFooter: true,
        title: "Hello Page"
    });

    $routeProvider
        .when(locationPath, {
            template: "<app-hello></app-hello>",
            resolve: {
                data: resolveHelloData
            }
        });


    resolveHelloData.$inject = ["$q","$http","locatorFactory","PageService","ContextService"];
    function resolveHelloData($q, $http, locatorFactory, PageService, ContextService){
        let deferred = $q.defer();

        $http.get( locatorFactory.hello ).then(function _resolve( response ){

            //store data in the context for a unique access
            ContextService.addFlashData("resolvedData", response.data, locationPath);

            PageService.ready( locationPath );
            deferred.resolve( response.data );
        });

        return deferred.promise;
    }
}