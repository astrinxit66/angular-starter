/**
 * Created by jean-marc on 24/07/2016.
 * @module app/components/hello/HelloController
 */
/*jshint esversion: 6*/
"use strict";

let $$ContextService = null;

export default class HelloController {

    constructor(ContextService){
        
        $$ContextService = ContextService;
    }
    
    $onInit(){

        /**
         * the bellow this.greeting var is furnished with data resolved in
         * hello.settings and stored as a flash data using ContextService.
         * The data was fetched from locatorFactory.hello endpoint which is 
         * handled by hello.mock.
         * @see app/components/hello/helloSettings
         * @see app/services/context/ContextService.addFlashData
         * @see app/services/locator/locatorFactory.hello
         * @see e2emock/mocks/HelloMock
         */
        this.greeting = $$ContextService.getFlashData("resolvedData");
    }
}
HelloController.$inject = ["ContextService"];