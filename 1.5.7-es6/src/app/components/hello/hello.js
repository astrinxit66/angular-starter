/**
 * Created by jean-marc on 24/07/2016.
 * @module app/components/hello/Hello
 */
/*jshint esversion: 6*/
"use strict";

import MODULE_NAME from "../../properties";

import HelloController from "./hello.controller";
import helloTemplate from "./hello.html!ng-template";

import helloSettings from "./hello.settings";

export default angular

    .module(MODULE_NAME.HELLO, [
        "ngRoute"
    ])

    .component("appHello", Object.assign(
        helloTemplate,
        {
            controller: HelloController
        }
    ))

    .config( helloSettings );