/**
 * Created by jean-marc on 19/07/2016.
 */

(function(){
    "use strict";

    module.exports = testInitializer;

    var
        //shortcut for plugins.config
        config = null,

        //shortcut for plugins.gulp
        gulp = null,

        //gulp plugin container, we deliberately list
        //the plugin components that we are using in this
        //file for code clarity
        plugins = {
            config: null,
            gulp: null,
            karmaServer: null,
            notify: null
        };

    function testInitializer( pluginCollection ){
        config = pluginCollection.config;
        gulp = pluginCollection.gulp;
        plugins = pluginCollection;

        //register test tasks
        gulp.task("test", testTask);
    }
    
    function testTask( done ){
        new plugins.karmaServer( {configFile: config.path.project +"/karma.conf.js"}, _whenDone).start();

        function _whenDone(){
            gulp.src("").pipe( plugins.notify("Unit Test done") );
            done();
        }
    }
})();