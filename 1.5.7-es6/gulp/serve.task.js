/**
 * Created by jean-marc on 19/07/2016.
 */

(function(){
    "use strict";

    module.exports = serveInitializer;

    
    var 
        //shortcut for plugins.config
        config = null,

        //shortcut for plugins.gulp
        gulp = null,

        //gulp plugin container, we deliberately list
        //the plugin components that we are using in this
        //file for code clarity
        plugins = {
            connect: null,
            defaultBrowser: null,
            gulp: null
        };

    function serveInitializer( pluginCollection ){
        config = pluginCollection.config;
        gulp = pluginCollection.gulp;
        plugins = pluginCollection;

        //register serve tasks
        gulp.task("serve:dev", ["validate", "test", "clean:dev", "package:dev"], serveDevTask);
        gulp.task("serve:prod", serveProdTask);
    }

    function serveDevTask(){
        var connectConf = config.connect.dev;
        
        //specify the root folder for the connect plugin
        connectConf.root = config.path.dist.dev;
        
        //track our app files and reload the browser on file changes
        //(except for less files)
        gulp.watch([config.path.app + config.path.subfiles, "!"+config.path.app + config.path.subfiles +".less"], {interval: 1000})
            .on("change", function reloadApp(){
                return gulp.src( config.path.dist.dev +"/index.html" )
                    .pipe( plugins.connect.reload() );
            });

        //mount the server, open a browser and inject the app onto it
        plugins.connect.server( connectConf );
        plugins.open("http://localhost:"+ connectConf.port, {app: plugins.defaultBrowser});
    }

    function serveProdTask(){

    }
})();