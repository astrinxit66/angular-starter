/**
 * Created by jean-marc on 20/07/2016.
 */

(function(){
    "use strict";

    module.exports = validateInitializer;

    var
        //shortcut for plugins.config
        config = null,

        //shortcut for plugins.gulp
        gulp = null,

        //gulp plugin container, we deliberately list
        //the plugin components that we are using in this
        //file for code clarity
        plugins = {
            config: null,
            gulp: null,
            notify: null
        };

    function validateInitializer( pluginCollection ){
        config = pluginCollection.config;
        gulp = pluginCollection.gulp;
        plugins = pluginCollection;

        //register serve tasks
        gulp.task("validate", validateTask);
    }
    
    function validateTask(){
        //check code quality through jshint
        var validation = gulp.src( config.path.app + config.path.subfiles +".js" )
            .pipe( plugins.jshint( config.jshint.rcfile ))
            .pipe( plugins.jshint.reporter( plugins.jshintStylishReporter ))
            .pipe( plugins.jshint.reporter( config.jshint.reportMode ));

        return validation;
    }
})();